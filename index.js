// console.log('Hello, World!');


// //Loops

	// While Loop

	/*
		
		Syntax:
			While(expression/condition) {
				statement
			}

	*/

	// Create a function called displayMsgToSelf()
		// -display a message to your past self in your console 10 times
		// Invoke the function 10 times

	 function displayMsgToSlef() {
	 	console.log("Don't text her back.");
	 }
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();
	 displayMsgToSlef();

	 // While loop

		 let count = 10;

		 while(count !== 0){
		 	console.log("Don't text her back.")
		 	count--;
		 };	

		 /*
			1st loop - Count 10
			2nd - 9
			3rd - 8 ...
			9th - 2
			10th- 1
	 		
	 		If there is no decrementation, the condition is always true, thus an infinite loop.
	 		Infinite loop will run your code block FOREVER until you stop it.

		 */

		 let count1 = 5;

		 while(count1 !== 0){
		 	console.log(count1);
		 	count1--;
		 };

		  let count2 = 1;

		 while(count2 <= 5){
		 	console.log(count2);
		 	count2++;
		 };

	 // Do While Loop

	 	// A do-while loop is like a while loop. But do-while loops gurantee that the code will be executed atleast once.

	 	/*
			Sytanx:
				do {
					statement
				} while(expression/condition)
	 	*/

	 	let doWhileCounter = 1;
		
		do {
	 		console.log(doWhileCounter);
	 		doWhileCounter++;
	 	} while(doWhileCounter <= 20)


	 	// let number = Number(prompt('Give me a number '));

	 	// do {
	 	// 	console.log("Do while: " + number);
	 	// 	//Increase the value of number by 1 after every iteration to stop the loop when it reaches 10.
	 	// 	//number = number + 1;
	 	// 	number += 1; //Addition assignment operator
	 	// } while(number < 10)


	 // For Loop
	 	/*
			It consists of three parts:
			1. Initialization - Value that will track the progression of the loop.
			2. Expression/Condition - that will be evaluated which will determine whether the loop will run one more time.
			3. finalExpression - Indicates how to advance the loop(++, --)

			Syntax:
				for(initialization; expression/condition; finalExpression) {
					statement
				}
	 	*/ 

	 	// Loop from 0-20
	 	for (let count = 0; count <= 20; count++) {
	 		console.log(count);
	 	}

	 	// For Loops accessing array Items

	 	let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Mango","Orange"];
		 console.log(fruits[2]);
		 	// .length property is also a property of an arra that shows the total number of items in an array.
		 console.log(fruits.length); // 5 total number if items in array
		 console.log(fruits[fruits.length-1]); // A more riliable way of checking the last item in an array. //arrayName.[arrayName.length-1]

	 		// Show all the items in an array in the console using loops:
	 		for(let index = 0; index < fruits.length; index++) {
	 			console.log(fruits[index]);
	 		}



	 // MINI ACTIVITY

	 	// Create an array with atleaste 6 items
	 		// Display all items in console EXCEPT for the last item

	 let favCountry = ["Philippines", "Japan", "Thailand", "Singapore", "United States", "Canada"]
	 
	 for (let index = 0; index < favCountry.length-1; index++) {
	 	console.log(favCountry[index]);
	 }



	 // For Loop accessing elements of a string
	 let myString = "alex";
	 	// .length it is also a property used in strings
	 console.log(myString.length);

	 	//Individual characters of a string can be access using it's index number. And it is also starts with 0. 
	 console.log(myString[0]);


	 // Will create a loop that will print out the inidividual letters of the myString

	 for (let x = 0; x < myString.length; x++) {
	 	console.log(myString[x])
	 } 

	 let myName = "Jane"

	 /*
		Create a loop that will print out the letters of the name individually and print out the number 3 isntead when the letter to be print out is a vowel.

	 */

	 for (let i = 0; i < myName.length; i++) {
	 	// if the character of your name is a vowel letter, display number "3"
	 	// console.log(myName[i])
	 	if(
	 		myName[i].toLowerCase() == "a" || 
	 		myName[i].toLowerCase() == "b" || 
	 		myName[i].toLowerCase() == "e" || 
	 		myName[i].toLowerCase() == "i" || 
	 		myName[i].toLowerCase() == "o" || 
	 		myName[i].toLowerCase() == "u" 
	 		){

	 		console.log(3);
	 	}
	 	else {
	 		//Print in all the non-vowel in the console
	 		console.log(myName[i])
	 	}
	 }


	 // Continue and Break Statements
	 	// Continue statement allows the code to go to the next iteration of the loop without finishin the execution of all statements in a code block.

	 	// Break statement is used to terminate the current loop once a match has been found.

	 	for(let count = 0; count <= 20; count++) {
	 		// if remainder is equal to 0. we will use the continue statement
	 		if(count % 2 === 0) {
	 			//Tells the code to continute to the next iteration.
	 			// This ignores all statements after the continue statement;
	 			continue;
	 		}
	 		// The current value of a number is printed out if the remainder is not equal to 0.
	 		console.log("Continue and Break: " + count)

	 		//If the current value of count is greater than 10, then use the break.
	 		if(count > 10){
	 			break;
	 		}
	 	}



	 // Create a loop that will iterate based on the length of the string
	 let name = "alexandro";

	 for (i = 0; i < name.length; i++) {

	 	console.log(name[i]);

	 	// if the vowe is equal to a, continue to the next iteration of the loop
	 	if(name[i].toLowerCase() === "a") {
	 		console.log("Continue to the next iteration.")
	 		continue;
	 	}
	 	if(name[i].toLowerCase() == "d"){
	 		break;
	 	}

	 }

